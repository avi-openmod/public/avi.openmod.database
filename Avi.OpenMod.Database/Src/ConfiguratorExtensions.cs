using Autofac;

using Avi.OpenMod.Database.Services;
using Avi.OpenMod.Database.Sqlite;

using Microsoft.Extensions.DependencyInjection;

using OpenMod.API.Ioc;
using OpenMod.API.Plugins;
using OpenMod.Core.Ioc.Extensions;
using OpenMod.EntityFrameworkCore.MySql;

// ReSharper disable UnusedMember.Global
// ReSharper disable UnusedType.Global

namespace Avi.OpenMod.Database; 

#pragma warning disable 1591
public static class ContainerBuilderExtensions {

#pragma warning restore 1591

    /// <summary>
    /// Sets up container for <see cref="AviDbContext"/> usage
    /// IMPORTANT: Use in global container <see cref="IContainerConfigurator.ConfigureContainer"/>
    /// </summary>
    public static void AddAviDatabaseSupport(this ContainerBuilder builder) {
        // add mysql and sqlite providers
        builder.RegisterType<PomeloMySqlConnectorResolver>()
            .AsSelf()
            .SingleInstance()
            .AutoActivate();
        builder.AddSqlitePclRawNativeLibLoader();
        // add config reader
        builder.RegisterType<AviDbConfigReader>()
            .As<IAviDbConfigReader>()
            .InstancePerLifetimeScope()
            .OwnedByLifetimeScope();
        // add db context configurator
        builder.RegisterType<AviDbContextConfigurator>()
            .As<IAviDbContextConfigurator>()
            .InstancePerDependency()
            .OwnedByLifetimeScope();
    }

    /// <summary>
    /// Configures the <see cref="AviDbContext"/> into component container
    /// IMPORTANT: Use in plugin container <see cref="IPluginContainerConfigurator.ConfigureContainer"/> when possible
    /// </summary>
    public static void AddAviDbContext<T>(this ContainerBuilder builder, ServiceLifetime serviceLifetime = ServiceLifetime.Transient)
        where T : AviDbContext {

        // add runtime stuff
        builder.RegisterType<RuntimeWorkDir>()
            .As<IAviDbWorkDirProvider>()
            .InstancePerDependency()
            .OwnedByLifetimeScope();

        // register the AviDbContext
        builder.RegisterType<T>()
            .PropertiesAutowired()
            .AsSelf()
            .WithLifetime(serviceLifetime)
            .OwnedByLifetimeScope();

    }

}