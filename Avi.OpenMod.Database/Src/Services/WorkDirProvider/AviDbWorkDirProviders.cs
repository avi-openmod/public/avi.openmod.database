using System;

using OpenMod.API;

namespace Avi.OpenMod.Database.Services; 

/// <summary>
/// Provides working directory for database config
/// </summary>
internal interface IAviDbWorkDirProvider {

    string WorkingDirectory { get; }

}
internal class RuntimeWorkDir : IAviDbWorkDirProvider {

    private readonly Lazy<IOpenModComponent> _lazyComponent;

    public RuntimeWorkDir(Lazy<IOpenModComponent> lazyComponent)
        => _lazyComponent = lazyComponent;

    public string WorkingDirectory => _lazyComponent.Value.WorkingDirectory;

}
internal class DesignTimeWorkDir : IAviDbWorkDirProvider {

    public string WorkingDirectory => Environment.CurrentDirectory;

}