using System;

using Autofac;

using Avi.OpenMod.Database.Sqlite.Services;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using OpenMod.API;
using OpenMod.Core.Helpers;

namespace Avi.OpenMod.Database.Services; 

internal class AviDbContextConfigurator : IAviDbContextConfigurator {

    private readonly ILogger<AviDbContextConfigurator> _logger;
    private readonly IAviDbConfigReader _configReader;
    private readonly IServiceProvider _serviceProvider;

    private EDatabaseType _databaseType;

    public AviDbContextConfigurator(
        ILogger<AviDbContextConfigurator> logger,
        IAviDbConfigReader configReader,
        IServiceProvider serviceProvider
    ) {
        _logger = logger;
        _configReader = configReader;
        _serviceProvider = serviceProvider;
    }

    public void Configure(AviDbContext context, DbContextOptionsBuilder optionsBuilder) {

        _databaseType = GetDatabaseType(context);
        switch (_databaseType) {
            case EDatabaseType.MySQL: {
                optionsBuilder.UseMySql(_configReader.ConnectionString,
                    options => options.MigrationsHistoryTable(context.MigrationsTableName));
                break;
            }

            case EDatabaseType.SQLite: {

                optionsBuilder.UseSqlite(_configReader.ConnectionString,
                    options => options.MigrationsHistoryTable(context.MigrationsTableName));

                var runtime = _serviceProvider.GetService<IRuntime>();
                if (runtime != null) {

                    var sqliteLoader = runtime.LifetimeScope.ResolveOptional<ISqlitePclRawNativeLibLoader>();
                    if (sqliteLoader == null) {
                        sqliteLoader = _serviceProvider.GetRequiredService<ISqlitePclRawNativeLibLoader>();
                        if (sqliteLoader != null) {
                            var component = _serviceProvider.GetService<IOpenModComponent>();
                            if (component != null) {
                                _logger.LogError(
                                    "{ServiceName} was resolved from non-global scope. Component '{ComponentId}' container is set up incorrectly",
                                    nameof(ISqlitePclRawNativeLibLoader), component.OpenModComponentId);
                            }
                        }
                    }

                    AsyncHelper.RunSync(async () => await sqliteLoader!.LoadNativeLibraryAsync("e_sqlite3"));

                } else {
                    _logger.LogWarning("Runtime unavailable, cannot load native SQLite libs");
                }

                break;
            }

            default: {
                throw new ArgumentOutOfRangeException();
            }
        }

    }

    // ReSharper disable once SuggestBaseTypeForParameter
    private static EDatabaseType GetDatabaseType(AviDbContext context) {

        string? className = context.GetType().Name;
        if (className.EndsWith("Sqlite", StringComparison.OrdinalIgnoreCase)) {
            return EDatabaseType.SQLite;
        }

        if (className.EndsWith("MySql", StringComparison.OrdinalIgnoreCase)) {
            return EDatabaseType.MySQL;
        }

        throw new ArgumentException($"Can't resolve DB type from class name of DBContext {className}");
    }

}