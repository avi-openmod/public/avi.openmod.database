using Microsoft.EntityFrameworkCore;

namespace Avi.OpenMod.Database.Services; 

internal interface IAviDbContextConfigurator {

    /// <summary>
    /// Configures the <see cref="AviDbContext"/> for correct provider
    /// </summary>
    void Configure(AviDbContext context, DbContextOptionsBuilder optionsBuilder);

}