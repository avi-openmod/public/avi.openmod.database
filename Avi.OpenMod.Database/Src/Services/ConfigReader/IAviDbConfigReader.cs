namespace Avi.OpenMod.Database.Services; 

/// <summary>
/// Service that provides values from database config
/// </summary>
public interface IAviDbConfigReader {

    /// <summary>
    /// Connection string
    /// </summary>
    string ConnectionString { get; }

    /// <summary>
    /// Database type
    /// </summary>
    // ReSharper disable once UnusedMemberInSuper.Global
    EDatabaseType DatabaseType { get; }

}