using System;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using OpenMod.Core.Helpers;

namespace Avi.OpenMod.Database.Services; 

internal class AviDbConfigReader : IAviDbConfigReader, IDisposable {

    public string ConnectionString {
        get {
            EDatabaseType dbType = DatabaseType;
            string str = Configuration.GetSection("connection_strings")[dbType.ToString().ToLower()];
            return dbType == EDatabaseType.SQLite ? str.Replace("{WorkingDirectory}", _workDirProvider.WorkingDirectory) : str;
        }
    }

    public EDatabaseType DatabaseType {
        get {

            string? dbTypeStr = Configuration["database_type"];
            if (!Enum.TryParse(dbTypeStr, true, out EDatabaseType dbType)) {
                throw new InvalidOperationException($"Database type not recognized: {dbTypeStr}");
            }

            return dbType;
        }
    }

    private IConfiguration? _configuration;
    private IConfiguration Configuration => _configuration ??= new ConfigurationBuilder()
        .SetBasePath(_workDirProvider.WorkingDirectory)
        .AddYamlFile("database.yaml", false)
        .Build();

    private readonly IAviDbWorkDirProvider _workDirProvider;

    public AviDbConfigReader(IServiceProvider serviceProvider)
        => _workDirProvider = serviceProvider.GetRequiredService<IAviDbWorkDirProvider>();

    public void Dispose()
        => _configuration?.DisposeSyncOrAsync();

}