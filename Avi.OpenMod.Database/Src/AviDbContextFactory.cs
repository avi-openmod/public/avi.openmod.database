using System;

using Avi.OpenMod.Database.Services;

using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.DependencyInjection;

namespace Avi.OpenMod.Database; 

/// <summary>
/// Extend this for each <see cref="AviDbContext"/> to support EF Core migration generation
/// </summary>
// ReSharper disable once UnusedType.Global
public class AviDbContextFactory<TContext> : IDesignTimeDbContextFactory<TContext> where TContext : AviDbContext {

    /// <inheritdoc />
    public TContext CreateDbContext(string[] args) {

        if (args == null) {
            throw new ArgumentNullException(nameof(args));
        }

        ServiceCollection serviceCollection = new();
        serviceCollection.AddLogging();

        serviceCollection.AddEntityFrameworkMySql();
        serviceCollection.AddEntityFrameworkSqlite();

        serviceCollection.AddDbContext<TContext>();

        serviceCollection.AddTransient<IAviDbWorkDirProvider, DesignTimeWorkDir>();
        serviceCollection.AddTransient<IAviDbConfigReader, AviDbConfigReader>();
        serviceCollection.AddTransient<IAviDbContextConfigurator, AviDbContextConfigurator>();

        return serviceCollection.BuildServiceProvider().GetRequiredService<TContext>();
    }

}