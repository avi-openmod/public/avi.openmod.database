using System;
using System.Reflection;

using Autofac;

using Avi.OpenMod.Database.Services;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using OpenMod.API.Ioc;
using OpenMod.API.Plugins;

namespace Avi.OpenMod.Database; 

/// <summary>
/// A base class for EF Core DBContext on OpenMod. You must create one context per database type and resolve
/// correct one depending on type provided by <see cref="IAviDbConfigReader"/>.
/// <br/>
/// MySQL provider class name must end with "MySql" and SQLite provider class name must end with "Sqlite" (both case insensitive).
/// <br/>
/// Example:
/// <code><![CDATA[
/// public abstract class MyContext : AviDbContext {
///     public DbSet<MyModel> Models { get; set; }
///     // ... constructors, method overrides here
/// }
/// public class MyContextSqlite : MyContext {
///     // ... constructors here
/// }
/// public class MyContextMySql : MyContext {
///     // ... constructors here
/// }]]>
/// </code>
/// This allows easy EF Core migration generation, for example:
/// <code>dotnet ef migrations add MigrationName --context MyContextSqlite --output-dir Migrations/Sqlite
/// dotnet ef migrations add MigrationName --context MyContextMySql --output-dir Migrations/MySql</code>
/// Setup:
/// <br/>
/// 1. You must extend <see cref="AviDbContextFactory{TContext}"/> for each context to be able to generate migrations
/// <br/>
/// 2. You must set up the container by:
/// <br/>
/// - using <see cref="ContainerBuilderExtensions.AddAviDatabaseSupport"></see>
/// in your <see cref="IContainerConfigurator"/>
/// <br/>
/// - using <see cref="ContainerBuilderExtensions.AddAviDbContext{T}(ContainerBuilder,ServiceLifetime)"/>
/// in your <see cref="IPluginContainerConfigurator"/> where possible (otherwise in <see cref="IContainerConfigurator"/>)
/// for each context
/// </summary>
public abstract class AviDbContext : DbContext {

    private readonly IServiceProvider _serviceProvider;
    private readonly IAviDbContextConfigurator _contextConfigurator;

    private ILogger? _logger;

    private ILogger Logger {
        get {

            if (_logger == null) {
                Type loggerGenericType = typeof(ILogger<>);
                Type loggerType = loggerGenericType.MakeGenericType(GetType());
                _logger = (ILogger) _serviceProvider.GetRequiredService(loggerType);
            }

            return _logger;
        }
    }

    private string? _id;

    /// <summary>
    /// ID of this context used for migrations and default table naming
    /// </summary>
    /// <exception cref="InvalidOperationException">if context can't be identified
    /// (assembly is not a plugin assembly and implementor doesn't override this property)</exception>
    public virtual string Id {
        get {

            if (_id != null) {
                return _id;
            }

            var pluginMetadataAttribute = GetType().Assembly.GetCustomAttribute<PluginMetadataAttribute>();
            if (pluginMetadataAttribute != null) {
                return _id = pluginMetadataAttribute.Id;
            }

            throw new InvalidOperationException(
                "Context can't be identified. Please override Id get-only property of your AviDbContext implementor or add PluginMetadataAttribute to your assembly");
        }
    }

    /// <summary>Gets the name of the migrations table.</summary>
    public virtual string MigrationsTableName => $"__{Id.Replace(".", "_")}{"_MigrationsHistory".ToLower()}";

    /// <summary>Gets the prefix for the tables.</summary>
    public virtual string TablePrefix => $"{Id.Replace(".", "_")}_";

    /// <inheritdoc />
    protected AviDbContext(IServiceProvider serviceProvider) {
        _contextConfigurator = serviceProvider.GetRequiredService<IAviDbContextConfigurator>();
        _serviceProvider = serviceProvider;
    }

    /// <inheritdoc />
    protected AviDbContext(DbContextOptions options, IServiceProvider serviceProvider) : base(options) {
        _contextConfigurator = serviceProvider.GetRequiredService<IAviDbContextConfigurator>();
        _serviceProvider = serviceProvider;
    }

    /// <inheritdoc />
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) {

        if (optionsBuilder.IsConfigured) {
            return;
        }

        var loggerFactory = _serviceProvider.GetRequiredService<ILoggerFactory>();
        optionsBuilder.UseLoggerFactory(loggerFactory);
        optionsBuilder.UseApplicationServiceProvider(_serviceProvider);

        _contextConfigurator.Configure(this, optionsBuilder);

    }

    /// <inheritdoc />
    protected override void OnModelCreating(ModelBuilder modelBuilder) {
        base.OnModelCreating(modelBuilder);
        ApplyTableNameConvention(modelBuilder);
    }

    /// <summary>
    /// Applies table name changes
    /// </summary>
    /// <param name="modelBuilder"></param>
    protected virtual void ApplyTableNameConvention(ModelBuilder modelBuilder) {

        if (string.IsNullOrEmpty(TablePrefix)) {
            return;
        }

        foreach (IMutableEntityType entityType in modelBuilder.Model.GetEntityTypes()) {

            string oldName = entityType.GetTableName();
            string newName = TablePrefix + oldName;
            if (newName == oldName) {
                continue;
            }

            Logger.LogDebug("Applying table name convention: {OldTableName} -> {NewTableName}", oldName, newName);
            entityType.SetTableName(newName);

        }

    }

}