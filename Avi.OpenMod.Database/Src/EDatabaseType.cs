// ReSharper disable InconsistentNaming

namespace Avi.OpenMod.Database; 

/// <summary>
/// Database type, supported by this library
/// </summary>
public enum EDatabaseType {

#pragma warning disable 1591
    MySQL,
    SQLite
#pragma warning restore 1591

}