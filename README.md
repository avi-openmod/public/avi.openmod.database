Avi.OpenMod.Database package
----------------------------

An OpenMod mini-framework that allows comfortable usage of MySQL/SQLite providers for EF Core.

#### Usage:

0. Install Nuget dependency `Avi.OpenMod.Database`.
1. Create DbContexts to share `DbSet`s between MySQL and SQLite variants:

```csharp
public abstract class MyDbContext : AviDbContext {
    public DbSet<MyModel> Models { get; set; }
    // ... constructors, method overrides here
}
public class MyDbContextSqlite : MyDbContext {
    // ... constructors here
}
public class MyDbContextMySql : MyDbContext {
    // ... constructors here
}
```

Use `IAviDbConfigReader` to create correct context at runtime:

```csharp
internal MyDbContext CreateDbContext() => _dbConfigReader.DatabaseType switch {
    EDatabaseType.MySQL => _serviceProvider.GetRequiredService<MyDbContextMySql>(),
    EDatabaseType.SQLite => _serviceProvider.GetRequiredService<MyDbContextSqlite>(),
    _ => throw new ArgumentOutOfRangeException()
};
```

Note: By default `DbContext`s are transient. This means you should create a context per database operation/transaction you're performing.

2. Enable lib support and register DBContexts in `IContainerConfigurator`:

```csharp
public class ContainerConfigurator : IContainerConfigurator {
    public void ConfigureContainer(IOpenModServiceConfigurationContext openModStartupContext, ContainerBuilder containerBuilder) {
        // ...
        // this section must be in IContainerConfigurator (NOT in IPluginContainerConfigurator)
        containerBuilder.AddAviDatabaseSupport();
        // ...
    }
}
```

Note: Use `IPluginContainerConfigurator` for OpenMod plugins - `context.ContainerBuilder.AddAviDatabaseSupport()`.

```csharp
public class ContainerConfigurator : IPluginContainerConfigurator {
    public void ConfigureContainer(IPluginServiceConfigurationContext context) {
        // ...
        // Note: this section should be in IPluginContainerConfigurator for plugins, otherwise put it in IContainerConfigurator (shown above)
        context.ContainerBuilder.AddDbContext<MyDbContextSqlite>();
        context.ContainerBuilder.AddDbContext<MyDbContextMySql>();
        // ...
    }
}
```

3. Create an  `AviDbContextFactory` for each DbContext:

```csharp
public class MyDbContextSQLiteFactory : AviDbContextFactory<MyDbContextSqlite> { }
public class MyDbContextMySQLFactory : AviDbContextFactory<MyDbContextMySql> { }
```

4. Install package `Microsoft.EntityFrameworkCore.Design` package with latest of v3 (v3.1.17 at the time of writing). Then create migrations from terminal in project directory:

```
dotnet ef migrations add MigrationName --context MyDbContextSqlite --output-dir Migrations/Sqlite
dotnet ef migrations add MigrationName --context MyDbContextMySql --output-dir Migrations/MySql
```

Note: create initial migrations and new ones every time database schema changes, more on it [here](https://docs.microsoft.com/en-us/ef/core/managing-schemas/migrations).

This package has the following package dependencies: `Avi.OpenMod.Database.Sqlite` for SQLite support, `OpenMod.EntityFrameworkCore.MySql` for Pomelo MySQL support.

Avi.OpenMod.Database.Sqlite package
-----------------------------------

This package contains a single service that can be used to load/unload SQLitePCLRaw native libraries in OpenMod environment.
Supported platforms: Windows, Linux, macOS.

#### Usage:

0. Install Nuget dependency `Avi.OpenMod.Database.Sqlite`.
1. Enable lib support in `IContainerConfigurator` (NOT in `IPluginContainerConfigurator`):

```csharp
public class ContainerConfigurator : IContainerConfigurator {
    public void ConfigureContainer(IOpenModServiceConfigurationContext openModStartupContext, ContainerBuilder containerBuilder) {
        // ...
        containerBuilder.AddSqlitePclRawNativeLibLoader();
        // ...
    }
}
```

3. Use service `ISqlitePclRawNativeLibLoader` to load/unload native libraries:

Libraries supported by this package:
* [SQLitePCLRaw.lib.e_sqlite3](https://www.nuget.org/packages/SQLitePCLRaw.lib.e_sqlite3/)
* [SQLitePCLRaw.lib.e_sqlcipher](https://www.nuget.org/packages/SQLitePCLRaw.lib.e_sqlcipher/)

Install one of the packages and load the corresponding library using `ISqlitePclRawNativeLibLoader.LoadNativeLibraryAsync`.

How it works:
The native library for corresponding platform is copied into `native` folder inside OpenMod runtime working directory. Then it is loaded using system-specific tools (`kernel32.dll` on Windows and `dl` on Unix).

License
=======

    Copyright 2021 aviadmini

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.