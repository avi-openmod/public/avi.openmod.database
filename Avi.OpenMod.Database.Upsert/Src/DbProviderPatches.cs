using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

using FlexLabs.EntityFrameworkCore.Upsert.Runners;

using HarmonyLib;

// ReSharper disable UnusedType.Global
// ReSharper disable UnusedMember.Local
// ReSharper disable InconsistentNaming

namespace Avi.OpenMod.Database.Upsert;

[HarmonyPatch]
internal static class PatchSqlite {

    private static readonly Regex _regex = new("^.*(-[0-9a-f]{6})$");

    [HarmonyTargetMethods]
    private static IEnumerable<MethodBase> TargetMethods() {
        Type baseType = typeof(UpsertCommandRunnerBase);
        Assembly asm = baseType.Assembly;
        return AccessTools.GetTypesFromAssembly(asm)
            .SelectMany(type => type.GetMethods())
            .Where(method => !method.IsAbstract &&
                             baseType.IsAssignableFrom(method.DeclaringType) &&
                             method.Name == nameof(UpsertCommandRunnerBase.Supports)
            );
    }

    [HarmonyPrefix]
    private static void Prefix(ref string __0) {
        if (_regex.Matches(__0).Count == 1) {
            __0 = __0[..^7];
        }
    }

}