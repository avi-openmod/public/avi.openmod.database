using Autofac;

using Avi.OpenMod.Database.Sqlite.Services;

using OpenMod.API.Ioc;

// ReSharper disable UnusedMember.Global

namespace Avi.OpenMod.Database.Sqlite;

#pragma warning disable 1591
// ReSharper disable once UnusedType.Global
public static class ContainerBuilderExtensions {

#pragma warning restore 1591

    /// <summary>
    /// Sets up container for SQLite usage
    /// IMPORTANT: Use in global container <see cref="IContainerConfigurator.ConfigureContainer"/>
    /// </summary>
    public static void AddSqlitePclRawNativeLibLoader(this ContainerBuilder builder)
        => builder.RegisterType<SqlitePclRawNativeLibLoaderNuget>()
            .As<ISqlitePclRawNativeLibLoader>()
            .SingleInstance()
            .OwnedByLifetimeScope();

}