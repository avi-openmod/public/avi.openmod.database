using System;

namespace Avi.OpenMod.Database.Sqlite.Native; 

internal interface INativeLibHandler {

    IntPtr LoadLib(string filePath);

    void UnloadLib(IntPtr handle);

    IntPtr GetExport(IntPtr handle, string name);

}