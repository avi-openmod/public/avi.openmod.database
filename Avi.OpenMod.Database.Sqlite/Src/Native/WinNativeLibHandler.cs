using System;
using System.ComponentModel;
using System.Runtime.InteropServices;

namespace Avi.OpenMod.Database.Sqlite.Native; 

internal class WinNativeLibHandler : INativeLibHandler {

    [DllImport("kernel32.dll", SetLastError = true)]
    private static extern IntPtr LoadLibrary(string lpFileName);

    [DllImport("kernel32.dll", SetLastError = true)]
    private static extern IntPtr GetProcAddress(IntPtr hModule, string procedureName);

    [DllImport("kernel32.dll", SetLastError = true)]
    private static extern bool FreeLibrary(IntPtr hModule);

    public IntPtr LoadLib(string filePath) {

        IntPtr result = LoadLibrary(filePath);

        if (result != IntPtr.Zero) {
            return result;
        }

        int error = Marshal.GetLastWin32Error();
        throw new Win32Exception($"LoadLibrary for {filePath} failed with error: {error}");
    }

    public void UnloadLib(IntPtr handle)
        => FreeLibrary(handle);

    public IntPtr GetExport(IntPtr handle, string name)
        => GetProcAddress(handle, name);

}