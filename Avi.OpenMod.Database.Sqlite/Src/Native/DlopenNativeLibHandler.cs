using System;
using System.Runtime.InteropServices;

// ReSharper disable IdentifierTypo
// ReSharper disable InconsistentNaming

namespace Avi.OpenMod.Database.Sqlite.Native; 

internal class DlopenNativeLibHandler : INativeLibHandler {

    private const int RTLD_NOW = 2;

    [DllImport("dl")]
    private static extern IntPtr dlopen(string dllToLoad, int flags);

    [DllImport("dl")]
    private static extern IntPtr dlsym(IntPtr hModule, string procedureName);

    [DllImport("dl")]
    private static extern int dlclose(IntPtr hModule);

    public IntPtr LoadLib(string filePath) {

        IntPtr result = dlopen(filePath, RTLD_NOW);

        if (result != IntPtr.Zero) {
            return result;
        }

        throw new Exception("dlopen for {filePath} failed");
    }

    public void UnloadLib(IntPtr handle)
        => dlclose(handle);

    public IntPtr GetExport(IntPtr handle, string name)
        => dlsym(handle, name);

}