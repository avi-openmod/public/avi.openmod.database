using System.Threading.Tasks;

using OpenMod.API.Ioc;

namespace Avi.OpenMod.Database.Sqlite.Services; 

/// <summary>
/// Service that manages loading of native SQLite libraries
/// </summary>
[Service]
public interface ISqlitePclRawNativeLibLoader {

    /// <summary>
    /// Loads an SQL native library for current platform
    /// </summary>
    /// <param name="nativeLibName">SQLite library name</param>
    Task LoadNativeLibraryAsync(string nativeLibName);

    /// <summary>
    /// Unloads a SQL native library managed by this loader
    /// </summary>
    /// <param name="nativeLibName">SQLite library name</param>
    /// <returns>true if library was unloaded, false otherwise</returns>
    bool UnloadNativeLibrary(string nativeLibName);

}