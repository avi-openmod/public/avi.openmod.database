using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

using Avi.OpenMod.Database.Sqlite.Native;

using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using MoreLinq;

using NuGet.Common;
using NuGet.Packaging;
using NuGet.Packaging.Core;
using NuGet.Versioning;

using OpenMod.API;
using OpenMod.API.Ioc;
using OpenMod.NuGet;

using SQLitePCL;

namespace Avi.OpenMod.Database.Sqlite.Services; 

[ServiceImplementation(Lifetime = ServiceLifetime.Singleton)]
// ReSharper disable once UnusedType.Global
internal class SqlitePclRawNativeLibLoaderNuget : ISqlitePclRawNativeLibLoader, IDisposable {

    private readonly ILogger<SqlitePclRawNativeLibLoaderNuget> _logger;
    private readonly IRuntime _runtime;
    private readonly NuGetPackageManager _packageManager;

    private readonly Dictionary<string, IntPtr> _loadedLibs = new();

    public SqlitePclRawNativeLibLoaderNuget(
        ILogger<SqlitePclRawNativeLibLoaderNuget> logger,
        IRuntime runtime,
        NuGetPackageManager packageManager
    ) {
        _logger = logger;
        _runtime = runtime;
        _packageManager = packageManager;
    }

    public void Dispose() {
        INativeLibHandler libHandler = GetLibLoader();
        _loadedLibs.ForEach(kvp => {
            if (kvp.Value != IntPtr.Zero) {
                libHandler.UnloadLib(kvp.Value);
            }
        });
    }

    private static bool IsFileLocked(FileInfo file) {
        try {
            using FileStream stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None);
            stream.Close();
        } catch (IOException) {
            //the file is unavailable because it is:
            //still being written to
            //or being processed by another thread
            //or does not exist (has already been processed)
            return true;
        }

        //file is not locked
        return false;
    }

    public async Task LoadNativeLibraryAsync(string nativeLibName) {

        if (_loadedLibs.TryGetValue(nativeLibName, out IntPtr libPtr) && libPtr != IntPtr.Zero) {
            SetProvider(GetLibLoader(), nativeLibName, libPtr);
            return;
        }

        string libDirOnDisk = Path.Combine(_runtime.WorkingDirectory, "native");
        string libPathOnDisk = Path.Combine(libDirOnDisk, GetNativeLibFileName(nativeLibName));
        var libFileInfo = new FileInfo(libPathOnDisk);
        if (libFileInfo.Exists) {

            if (IsFileLocked(libFileInfo)) {
                _logger.LogError("Library is not loaded but it's file exists and is locked. YOLO-ing SetProvider");
                SetProviderFromFile(nativeLibName, libPathOnDisk);
                return;
            }

            _logger.LogDebug("Overriding SQLite lib '{Lib}' file from nuget package", nativeLibName);

        } else {
            _logger.LogDebug("Loading SQLite lib '{Lib}' file from nuget package", nativeLibName);
        }

        string libPackageName = GetLibNugetPackage(nativeLibName);

        PackageIdentity? identity = await _packageManager.GetLatestPackageIdentityAsync(libPackageName);

        if (identity == null) {
            throw new InvalidOperationException($"Package '{libPackageName}' must be installed to load SQLite native library");
        }

        if (identity.Version < new SemanticVersion(2, 0, 2)) {
            throw new InvalidOperationException($"Package '{libPackageName}' version below 2.0.2 is not supported");
        }

        string packageFile = _packageManager.GetNugetPackageFile(identity);

        using var packageReader = new PackageArchiveReader(packageFile);
        _logger.LogDebug("Extracting SQLite native lib'{Lib}' from NuGet package at {PackageFileName}",
            nativeLibName, Path.GetFileName(packageFile));

        string libPathInPackage = GetLibPathInPackage(nativeLibName);

        var nugetLogger = new NullLogger();

        packageReader.ExtractFile(libPathInPackage, libPathOnDisk, nugetLogger);

        SetProviderFromFile(nativeLibName, libPathOnDisk);

    }

    public bool UnloadNativeLibrary(string nativeLibName) {

        if (!_loadedLibs.TryGetValue(nativeLibName, out IntPtr libPtr) || libPtr == IntPtr.Zero) {
            return false;
        }

        GetLibLoader().UnloadLib(libPtr);

        return true;
    }

    private void SetProviderFromFile(string nativeLibName, string libPathOnDisk) {

        _logger.LogDebug("Loading SQLite native lib '{Lib}'", nativeLibName);

        INativeLibHandler libHandler = GetLibLoader();

        IntPtr resultLibPtr = libHandler.LoadLib(libPathOnDisk);

        SetProvider(libHandler, nativeLibName, resultLibPtr);

        _loadedLibs[nativeLibName] = resultLibPtr;

    }

    private void SetProvider(INativeLibHandler libHandler, string nativeLibName, IntPtr libPtr) {

        _logger.LogDebug("SQLite setup for native lib '{Lib}'", nativeLibName);

        SQLite3Provider_dynamic_cdecl.Setup(nativeLibName, new GetFunctionPointerWrapper(libHandler, libPtr));
        raw.SetProvider(new SQLite3Provider_dynamic_cdecl());

    }

    private class GetFunctionPointerWrapper : IGetFunctionPointer {

        private readonly INativeLibHandler _nativeLibHandler;
        private readonly IntPtr _dll;

        public GetFunctionPointerWrapper(INativeLibHandler nativeLibHandler, IntPtr dll) {
            _nativeLibHandler = nativeLibHandler;
            _dll = dll;
        }

        public IntPtr GetFunctionPointer(string name)
            => _nativeLibHandler.GetExport(_dll, name);

    }

    private static string GetLibNugetPackage(string nativeLibName)
        => $"SQLitePCLRaw.lib.{nativeLibName}";

    private static INativeLibHandler GetLibLoader() {

        if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows)) {
            return new WinNativeLibHandler();
        }

        if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux) || RuntimeInformation.IsOSPlatform(OSPlatform.OSX)) {
            return new DlopenNativeLibHandler();
        }

        throw new PlatformNotSupportedException();

    }

    #region Lib path

    private static string GetLibRidFront() {

        if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows)) {
            return "win";
        }

        if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux)) {
            return "linux";
        }

        if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX)) {
            return "osx";
        }

        throw new PlatformNotSupportedException();

    }

    private static string GetLibRidBack() {
        try {
            // attempt to use RuntimeInformation
            return (int) RuntimeInformation.OSArchitecture switch {
                0 => "x86",
                1 => IntPtr.Size != 8 ? "x86" : "x64",
                2 => "arm",
                3 => "arm64",
                _ => throw new PlatformNotSupportedException()
            };
        } catch (Exception) {
            // best effort determine 32/64bit and if arch is ARM 
            typeof(object).Module.GetPEKind(out PortableExecutableKinds _, out ImageFileMachine machine);
            return machine == ImageFileMachine.ARM
                ? Environment.Is64BitProcess
                    ? "arm64"
                    : "arm"
                : Environment.Is64BitProcess
                    ? "x64"
                    : "x86";
        }
    }

    private static string GetLibRid()
        => $"{GetLibRidFront()}-{GetLibRidBack()}";

    private static string GetNativeLibFileName(string nativeLibName) {

        if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows)) {
            return $"{nativeLibName}.dll";
        }

        if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux)) {
            return $"lib{nativeLibName}.so";
        }

        if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX)) {
            return $"lib{nativeLibName}.dylib";
        }

        throw new PlatformNotSupportedException();

    }

    private static string GetLibPathInPackage(string nativeLibName)
        => $"runtimes/{GetLibRid()}/native/{GetNativeLibFileName(nativeLibName)}";

    #endregion

}